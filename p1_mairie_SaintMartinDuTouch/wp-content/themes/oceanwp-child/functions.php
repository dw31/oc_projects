<?php

/*
* load parent than child css
*/

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri().'/style.css');
    wp_enqueue_style('child-style', get_stylesheet_directory_uri().'/style.css', array('parent-style'));
}

///////////////////////////////////////////////////////////////////////////////////

/*
* enable posts thumbnails
*/
if (function_exists('add_theme_support')) 
{
  add_theme_support('post-thumbnails');
}

///////////////////////////////////////////////////////////////////////////////////

/*
* enable media uploading for contributors
*/

if ( current_user_can('contributor') && !current_user_can('upload_files') )
    add_action('admin_init', 'allow_upload_contributors');

function allow_upload_contributors() {
    $contributor = get_role('contributor');
    $contributor->add_cap('upload_files');
}

///////////////////////////////////////////////////////////////////////////////////

function rm_updt(){global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);}
add_filter('pre_site_transient_update_core','rm_updt');
add_filter('pre_site_transient_update_plugins','rm_updt');
add_filter('pre_site_transient_update_themes','rm_updt');

?>
