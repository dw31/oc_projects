<?php

// BEGIN iThemes Security - Ne pas modifier ou supprimer cette ligne
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Désactiver l’éditeur de fichier - Sécurité > Réglages > Modifications de WordPress > Éditeur de fichier
// END iThemes Security - Ne pas modifier ou supprimer cette ligne

/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'p1oc');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/*
* keep in trash only one week
*/
define ('EMPTY_TRASH_DAYS', 7);

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'caSR9%Iys,U9_C^~YQ7+VK g,iRp$s+}#6*6|4L} hIOn&nk-2h[J8v[Vd_QPaA+');
define('SECURE_AUTH_KEY',  '}Rm&Zc}w)1pnGhLRkXK<2^{* e=$@xSs!|*9d$}F3zy/<(AbGn&oRiejEGk 1jI@');
define('LOGGED_IN_KEY',    'V#~0QiJ c_d#)FBs&6v#9cSv Kr[YW4-^3E06Ntl`=#ANc85SmTD]CI`=[f^ogp-');
define('NONCE_KEY',        ':[rOA8]0HK7AK#5WLF-q=1{oG/5{MLdpW~11rEd{{iq(VC~r=?4%@=;mLt/PqbKM');
define('AUTH_SALT',        't7|<gz~|>an3?NDUd/I{9Dn0.(}f?Z7|m8<[(_Nx_[lj /T ):Zt-TycjZw(:Uo*');
define('SECURE_AUTH_SALT', '4/|M5:U0(1#;SJ]}*y=(`2lcCq`bEyo*nF!U.sQ+8_ZE+V!>Far SNB/G.#33j`<');
define('LOGGED_IN_SALT',   'q9?3*Egg~Fkqb5$=EM2V))-lR<$:BmMliV#NnF3{1c?Fr]l6(dC1g+Bitq6blsJ;');
define('NONCE_SALT',       'o ptf5Tv ^Hc7R7/nPSsg=19jD2gG[SJZWQ3bI^(vEA[v?;g^-:{/D4#C@,xX&DW');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'p1oc_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
